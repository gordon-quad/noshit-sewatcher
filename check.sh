#!/bin/bash
# no shit sewatcher

if [ $# -ne 1 ];
then
    echo "Usage: $(basename "$0") <se-save-file-path>"
    exit
fi

FILE=$1

unzip -c -P GarfieldJonesCat "$FILE" | grep -aPo "This is the base file for creation the random planets\. \K." | dd count=1 bs=1 status=none | hexdump -ve '1/1 "%d"'
echo
