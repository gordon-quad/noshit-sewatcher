#!/bin/bash
# no shit sewatcher

if [ $# -lt 2 ];
then
    echo "Usage: $(basename "$0") <se-save-file-path> <player-number> [notify message]"
    exit
fi

FILE=$1
FILENAME=$(basename "$FILE")
DIRNAME=$(dirname "$FILE")
PLAYER=$2
shift 2
TMP="$*"
MESSAGE=${TMP:=Your turn, you lazy fuck!}

inotifywait -e CLOSE_WRITE,MOVED_TO -q -r -m "$DIRNAME" | while read -r LINE;
do
    EVENT_FILE=$(echo "$LINE" | cut -d' ' -f3)
    if [ "$EVENT_FILE" != "$FILENAME" ];
    then
        continue
    fi
    # SE writes save file in two operations, first one is garbage, second one - actual zip file
    # make sure we skip first
    [ "$(dd if="$FILE" count=2 bs=1 status=none | tr '[:cntrl:]' '_')" != "PK" ] && continue
    CURRENT=$(unzip -c -P GarfieldJonesCat "$FILE" | grep -aPo "This is the base file for creation the random planets\. \K." | dd count=1 bs=1 status=none | hexdump -ve '1/1 "%d"')
    if [ "$CURRENT" == "" ];
    then
        echo "Something is wrong with parsing save file!" >&2
        continue
    fi
    [ "$CURRENT" -eq "$PLAYER" ] && aplay notify.wav &>/dev/null &
    [ "$CURRENT" -eq "$PLAYER" ] && herbe "$MESSAGE" # Change here to your favourite notifier
    sleep 0.1 # safety sleep
done
